FROM ubuntu:latest

RUN apt-get update \
    && apt-get install -y rclone \
    && apt-get install --reinstall -y ca-certificates \
    && rm -rf /var/lib/apt/lists/*
